package engine

import (
	"bitbucket.org/lygo/lygo_commons/lygo_regex"
	"bitbucket.org/lygo/lygo_ext_http/httpclient"
	"net/url"
	"regexp"
	"strings"
)

// ---------------------------------------------------------------------------------------------------------------------
// 	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func ShortUrlsInMessage(text string) string {
	links := lygo_regex.Links(text)
	if len(links) > 0 {
		for _, link := range links {
			short := ShortUrl(link)
			text = strings.ReplaceAll(text, link, short)
		}
	}
	return text
}

func ShortUrl(uri string) string {
	client := new(httpclient.HttpClient)
	callUrl := "http://tinyurl.com/api-create.php?url=" + url.QueryEscape(uri)
	response, err := client.Get(callUrl)
	if nil == err {
		return string(response.Body)
	}
	return uri
}

func ClearPhone(text string) string {
	regex := regexp.MustCompile("[^\\+0-9]")
	return regex.ReplaceAllString(text, "")
}

func CheckPrefix(text string) string {
	if !strings.HasPrefix(text, "+"){
		return "+" + text
	}
	return text
}

func Render(text string, context map[string]string) string {
	tags := lygo_regex.BetweenBraces(text)
	for _, tag := range tags {
		if v, b := context[tag[1]]; b {
			text = strings.ReplaceAll(text, tag[0], v)
		}
	}
	return text
}
