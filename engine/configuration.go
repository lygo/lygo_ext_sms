package engine

import (
	"bitbucket.org/lygo/lygo_commons/lygo_json"
)

// ---------------------------------------------------------------------------------------------------------------------
// 	SMSProvider
// ---------------------------------------------------------------------------------------------------------------------

type SMSProvider struct {
	Driver   string            `json:"driver"`
	Method   string            `json:"method"`
	Endpoint string            `json:"endpoint"`
	Params   map[string]string `json:"params"`
	Headers  map[string]string `json:"headers"`
}

// ---------------------------------------------------------------------------------------------------------------------
// 	SMSConfiguration
// ---------------------------------------------------------------------------------------------------------------------

type SMSConfiguration struct {
	Enabled      bool                    `json:"enabled"`
	AutoShortUrl bool                    `json:"auto-short-url"`
	Providers    map[string]*SMSProvider `json:"providers"`
}

func NewSMSConfigurationFromFile(filename string) (*SMSConfiguration, error) {
	text, err := lygo.IO.ReadTextFromFile(filename)
	if nil != err {
		return nil, err
	}
	return NewSMSConfigurationFromString(text)
}

func NewSMSConfigurationFromMap(m map[string]interface{}) (*SMSConfiguration, error) {
	text := lygo_json.Stringify(m)
	return NewSMSConfigurationFromString(text)
}

func NewSMSConfigurationFromString(text string) (*SMSConfiguration, error) {
	var instance SMSConfiguration
	err := lygo_json.Read(text, &instance)
	if nil != err {
		return nil, err
	}
	return &instance, nil
}

// ---------------------------------------------------------------------------------------------------------------------
// 	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------
