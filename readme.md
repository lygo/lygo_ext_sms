# SMS Gateway Client #

![](icon.png)

`lygo_ext_sms` is a client implementation to send SMS using gateways.

## Dependencies ##

```
go get -u bitbucket.org/lygo/lygo_commons
go get -u bitbucket.org/lygo/lygo_ext_http
```

## How to Use ##

```
go get -u bitbucket.org/lygo/lygo_ext_sms@v0.1.5
```

## Versioning ##

Sources are versioned using git tags:

```
git tag v0.1.5
git push origin v0.1.5
```

