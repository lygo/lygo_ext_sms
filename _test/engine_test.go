package _test

import (
	"bitbucket.org/lygo/lygo_ext_sms"
	"bitbucket.org/lygo/lygo_ext_sms/engine"
	"fmt"
	"testing"
)

func TestSender(t *testing.T) {
	config, err := engine.NewSMSConfigurationFromFile("./settings.prod.json")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	sender := lygo_ext_sms.NewSMSEngine(config)
	resp, err := sender.SendMessage("smshosting",
		"Check this: https://gianangelogeminiani.me", "+39......", "ANGELO")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(resp)
}

func TestSkebby(t *testing.T) {
	config, err := engine.NewSMSConfigurationFromFile("./settings.skebby.json")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	sender := lygo_ext_sms.NewSMSEngine(config)
	resp, err := sender.SendMessage("skebby",
		"Check this: gianangelogeminiani . me", "393477857785", "ANGELO")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(resp)
}