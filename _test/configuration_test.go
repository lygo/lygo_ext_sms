package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_ext_sms/engine"
	"fmt"
	"testing"
)

func TestConfiguration(t *testing.T) {
	config, err := engine.NewSMSConfigurationFromFile("./settings.prod.json")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(lygo_json.Stringify(config))
}
