package lygo_ext_sms

import (
	"bitbucket.org/lygo/lygo_commons/lygo_json"
	"bitbucket.org/lygo/lygo_ext_sms/engine"
	"errors"
	"strings"
)

// ---------------------------------------------------------------------------------------------------------------------
// 	c o n s t
// ---------------------------------------------------------------------------------------------------------------------

var (
	MismatchConfigurationError = errors.New("mismatch_configuration")
	EngineNotEnabledError      = errors.New("engine_not_enabled")
	ProviderNotFoundError      = errors.New("provider_not_found")
)

// ---------------------------------------------------------------------------------------------------------------------
// 	t y p e
// ---------------------------------------------------------------------------------------------------------------------

type SMSEngine struct {
	configuration *engine.SMSConfiguration
}

func NewSMSEngine(settings ...interface{}) *SMSEngine {
	instance := new(SMSEngine)
	instance.configuration = new(engine.SMSConfiguration)
	if len(settings) > 0 {
		p1 := settings[0]
		if conf, b := p1.(*engine.SMSConfiguration); b {
			instance.configuration = conf
		} else if provider, b := p1.(*engine.SMSProvider); b {
			instance.configuration.Providers[provider.Driver] = provider
		} else {
			instance.tryLoadSettings(p1)
		}
	}

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
// 	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *SMSEngine) ProviderNames() []string {
	result := make([]string, 0)
	if nil != instance.configuration {
		for k, _ := range instance.configuration.Providers {
			result = append(result, k)
		}
	}
	return result
}

func (instance *SMSEngine) SendMessage(providerName, message, to, from string) (string, error) {
	if nil != instance && nil != instance.configuration {
		if instance.configuration.Enabled {
			provider, err := instance.getDriver(providerName)
			if nil != err {
				return "", err
			}
			return provider.Send(message, to, from)
		}
		return "", EngineNotEnabledError
	}
	return "", MismatchConfigurationError
}

// ---------------------------------------------------------------------------------------------------------------------
// 	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *SMSEngine) tryLoadSettings(item interface{}) {
	text := lygo_json.Stringify(item)
	if len(text) > 0 {
		// test settings
		var config engine.SMSConfiguration
		err := lygo_json.Read(text, &config)
		if nil == err && len(config.Providers) > 0 {
			instance.configuration = &config
			return
		}
		// test a provider
		var provider engine.SMSProvider
		err = lygo_json.Read(text, &provider)
		if nil == err && len(provider.Driver) > 0 {
			instance.configuration.Providers[provider.Driver] = &provider
			return
		}
	}
}

func (instance *SMSEngine) getDriver(name string) (driver engine.IDriver, err error) {
	provider, err := instance.getProvider(name)
	if nil != err {
		return nil, err
	}

	switch strings.ToLower(provider.Driver) {
	case "skebby":
		driver = engine.NewDriverSkebby(instance.configuration.AutoShortUrl, provider)
	default:
		driver = engine.NewDriverGeneric(instance.configuration.AutoShortUrl, provider)
	}

	return driver, nil
}

func (instance *SMSEngine) getProvider(name string) (*engine.SMSProvider, error) {
	if len(name) > 0 {
		if _, b := instance.configuration.Providers[name]; !b {
			return nil, ProviderNotFoundError
		}
		provider := instance.configuration.Providers[name]
		return provider, nil
	} else {
		names := instance.ProviderNames()
		if len(names) > 0 {
			firstName := names[0]
			if len(firstName) > 0 {
				return instance.getProvider(firstName)
			}
		}
	}
	return nil, ProviderNotFoundError
}
