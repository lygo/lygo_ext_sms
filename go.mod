module bitbucket.org/lygo/lygo_ext_sms

go 1.15

require (
	bitbucket.org/lygo/lygo_commons v0.1.104
	bitbucket.org/lygo/lygo_events v0.1.5 // indirect
	bitbucket.org/lygo/lygo_ext_http v0.1.19
	github.com/andybalholm/brotli v1.0.3 // indirect
	github.com/gofiber/fiber/v2 v2.13.0 // indirect
	github.com/klauspost/compress v1.13.1 // indirect
	github.com/valyala/fasthttp v1.27.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20210616094352-59db8d763f22 // indirect
)
